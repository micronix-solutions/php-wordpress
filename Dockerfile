# References:
# https://docs.docker.com/engine/examples/running_ssh_service/
FROM php:7.4-fpm-alpine
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/
RUN chmod uga+x /usr/local/bin/install-php-extensions && sync
RUN install-php-extensions exif gd imagick mysqli sockets gettext mcrypt opcache redis memcached bcmath pdo_mysql zip
