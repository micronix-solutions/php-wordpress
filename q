[32mens34: connected to Wired connection 2[0m
	"Intel 82545EM"
	ethernet (e1000), 00:0C:29:3A:8A:76, hw, mtu 1500
	ip4 default, ip6 default
	inet4 10.99.99.20/24
	route4 0.0.0.0/0
	route4 10.99.99.0/24
	inet6 2600:6c5e:c7f:ae89:b584:ed6d:4cdf:d3b5/64
	inet6 fe80::b021:6740:5e0f:3474/64
	route6 2600:6c5e:c7f:ae89::/64
	route6 ::/0
	route6 fe80::/64
	route6 ff00::/8

[32mens33: connected to Wired connection 1[0m
	"Intel 82545EM"
	ethernet (e1000), 00:0C:29:3A:8A:6C, hw, mtu 1500
	inet4 172.173.174.3/24
	route4 172.173.174.0/24
	inet6 fe80::bffe:ef58:a9c2:234a/64
	route6 fe80::/64
	route6 ff00::/8

[32mens35: connected to Wired connection 3[0m
	"Intel 82545EM"
	ethernet (e1000), 00:0C:29:3A:8A:80, hw, mtu 1500
	inet4 10.98.98.12/24
	route4 0.0.0.0/0
	route4 10.98.98.0/24
	route4 10.0.0.0/8
	route4 156.65.0.0/16
	inet6 fe80::b225:7b35:7b18:6b68/64
	route6 fe80::/64
	route6 ::/0
	route6 ff00::/8

[2mdocker0: unmanaged[0m
	"docker0"
	bridge, 02:42:73:ED:2F:97, sw, mtu 1500

[2mlo: unmanaged[0m
	"lo"
	loopback (unknown), 00:00:00:00:00:00, sw, mtu 65536

DNS configuration:
	servers: 10.99.99.254 127.0.0.1
	interface: ens34

	servers: 2600:6c5e:c7f:ae89:6238:e0ff:fe9c:4a22
	interface: ens34

	servers: 172.173.174.1 127.0.0.1
	domains: localdomain
	interface: ens33

	servers: 10.98.98.1 127.0.0.1
	domains: localdomain
	interface: ens35

Use "nmcli device show" to get complete information about known devices and
"nmcli connection show" to get an overview on active connection profiles.

Consult nmcli(1) and nmcli-examples(7) manual pages for complete usage details.
